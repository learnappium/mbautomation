package mbTests;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;

import mbPages.FrontPage;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import driverconfig.InitDriver;

public class MBLoginTest {
	
	public AndroidDriver driver;
	public InitDriver init = new InitDriver();
	
	//init = new InitDriver();
	
	@BeforeTest
	public void setupTest() throws IOException, InterruptedException{
		
		
		init.driverservice().start();		
	    driver=	init.setUpDriver("Android", "Android", "127.0.0.1", "4723");
	   
		
	}
	
	@Test
	public void LoginTest() throws InterruptedException{
		
		System.out.println("Test has been started");
		
	new  FrontPage(driver)
		.tapOnLocalityTextBox()
		.enterSearchCriteria()
		.selectSuggestion()
		.tapOnSearchDone()
		;
		//driver.pullFile(remotePath)
		Thread.sleep(10000L);
		
	}
	
	@AfterTest(alwaysRun=true)
	public void tearDown(){
		driver.quit();
		init.driverservice().stop();
	}
}
