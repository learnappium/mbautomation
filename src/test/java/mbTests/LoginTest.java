package mbTests;

import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import driverconfig.InitDriver;

public class LoginTest {
	
	public AndroidDriver driver;
	public InitDriver init = new InitDriver();
	//init = new InitDriver();
	
	@BeforeTest
	public void setupTest() throws IOException, InterruptedException{
		
		
		init.driverservice().start();
		
	driver=	init.setUpDriver("Android", "Android", "127.0.0.1", "4723", "D:\\Softwares\\sdk\\platform-tools\\MagicBricksPropertySearch_v6.2.5_apkpure.com.apk");
		
	}
	
	@Test
	public void LoginTest() throws InterruptedException{
		
		System.out.println("Test has been started");
		//driver.pullFile(remotePath)
		Thread.sleep(10000L);
		
	}
	
	@Test
	  public void pushFileTest() {
	    String str = "The eventual code is no more than the deposit of your understanding. ~E. W. Dijkstra";
	    
	    File file = new  File("LoadingScreen_iPhone.PNG");
	    byte[] data = file.getAbsolutePath().getBytes(Charset.forName("UTF-8"));          //.getBytes();
	    		//getBytes(Charset.forName("UTF-8"));
	    byte[] bFile = new byte[(int) file.length()];
	    String imageDataString = Base64.encodeBase64String(bFile);
	   byte[] bytearray = Base64.decodeBase64(imageDataString);
	    driver.pushFile("/sdcard/DCIM/appium.PNG", bytearray);
	    byte[] returnData = driver.pullFile("/sdcard/DCIM/appium.jpg");
	    
	    
	    //String returnDataDecoded = new String(returnData, StandardCharsets.UTF_8);
	    Assert.assertEquals(data, returnData);
	    
	    driver.pushFile("D://Appium//LoadingScreen_iPhone.png", returnData);
	    
	   
	  }
	
	@AfterTest
	public void tearDown(){
		driver.quit();
		init.driverservice().stop();
	}
	
	

}
