package mbPages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.PageFactory;

public class SearchLocationpage {
public AndroidDriver<MobileElement> driver;
	
	public SearchLocationpage(AndroidDriver<MobileElement> driver){
		
			this.driver=driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
			// TODO Auto-generated constructor stub
		}
	
	/*@AndroidFindBy(name="City, Locality, Project")
	private MobileElement searchCriteriaTextBox;*/
	
	@AndroidFindBy(xpath="android.widget.Button[@text=�Cancel��]//..//android.widget.EditText")
	private MobileElement searchCriteriaTextBox;
	
	@AndroidFindBy(xpath="//android.widget.ListView[1]/android.widget.LinearLayout/android.widget.TextView[@text='Locality']//..//android.widget.TextView[@text='Airoli, Navi Mumbai']")
	private MobileElement selectFromSuggestion;
	
	@AndroidFindBy(name="com.timesgroup.magicbricks:id/done_button")
	private MobileElement doneButton;
	
	
	public SearchLocationpage enterSearchCriteria(){
		searchCriteriaTextBox.sendKeys("mum");
		return this;
	}
	
	public SearchLocationpage selectSuggestion(){
		driver.tap(1, selectFromSuggestion, 1);
		return this;
	}
	
	public FrontPage tapOnSearchDone(){
		driver.tap(1, selectFromSuggestion, 1);
		return new FrontPage(driver);
	}

}
