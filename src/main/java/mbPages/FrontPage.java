package mbPages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.android.uiautomator.core.UiSelector;

public class FrontPage {
	
	public AndroidDriver<MobileElement> driver;
	
	public FrontPage(AndroidDriver<MobileElement> driver){
		
			this.driver=driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
			// TODO Auto-generated constructor stub
		}
	
	//@AndroidFindBy(id="com.timesgroup.magicbricks:id/locality_text_view")
	//private MobileElement localityTextView;		
	
	@AndroidFindBy(uiAutomator="new UiSelector().className('android.widget.TextView').textContains('City, Locality, Project')")
	private MobileElement localityTextView;	
	

	
	public SearchLocationpage tapOnLocalityTextBox(){	
		try{
			driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.timesgroup.magicbricks:id/locality_text_view']")).click();
			
		}catch(Exception e){			
			driver.findElement(By.xpath("//android.widget.TextView[@text='City, Locality, Project']")).click();
					}
		return new SearchLocationpage(driver);
	}
		
	}


