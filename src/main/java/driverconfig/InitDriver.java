package driverconfig;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

public class InitDriver {
	
	public static  AndroidDriver<MobileElement> driver;
	public static DesiredCapabilities cap;
	//AppiumDriverLocalService service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().withStartUpTimeOut(30, TimeUnit.SECONDS));

	Properties prop = new Properties();
	InputStream input = null;
	
	public String MB_PKG="com.timesgroup.magicbricks";
	public String MB_ACTIVITY="com.til.magicbricks.activities.SplashActivity";
	public String APP_NAME ="MagicBricksPropertySearch_v6.2.5_apkpure.com.apk";
	public String APP_PATH ="D:\\Softwares\\sdk\\platform-toolsMagicBricksPropertySearch_v6.2.5_apkpure.com.apk";
	
	
    public AndroidDriver setUpDriver( String plateform,String Device,String app,String ip , String port) throws IOException, InterruptedException {	
    	/*
    	 * define driver
    	 */
    cap = new DesiredCapabilities();
    
    cap.setCapability(MobileCapabilityType.PLATFORM_NAME, plateform);
    cap.setCapability(MobileCapabilityType.DEVICE_NAME, Device);
    cap.setCapability(MobileCapabilityType.APP, app);
    
    URL url = new URL("http://"+ip+":"+port+"/wd/hub");
	driver = new AndroidDriver<MobileElement>(url, cap);	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	return driver;	
    	
    }
    
    public AndroidDriver setUpDriver( String plateform,String Device,String ip,String port) throws IOException, InterruptedException {	
    	/*
    	 * define driver\
    	 * 
    	 * 
    	 */
    	cap = new DesiredCapabilities();
        
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, plateform);
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, Device);
        
        /*
         * LAUNCHING UNLOCKER FIRST
         * 
         * UNLOCKER PACKAGE:  io.appium.unlock
         * UNLOCKER ACTIVITY: Unlock
         */
        cap.setCapability(MobileCapabilityType.APP_PACKAGE, "io.appium.unlock");
        cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "Unlock");
        
        URL url = new URL("http://"+ip+":"+port+"/wd/hub");
    	driver = new AndroidDriver<MobileElement>(url, cap);	
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	
    	if(!driver.isAppInstalled(MB_PKG)){
    		System.out.println("App already not installed ..... installing app");
    		//driver.installApp("gov.mea.psp.apk");
    		driver.installApp("D:\\Appium\\apkfiles\\MagicBricksPropertySearch_v6.2.5_apkpure.com.apk");
    		System.out.println("App has been installed ");
    		System.out.println("Opening app");
    		driver.startActivity(MB_PKG, MB_ACTIVITY);    		
    		System.out.println("App lunched successfully");
    	}else{
    		
    		System.out.println("App already installed ..... Reseting app");
    		driver.resetApp();    		
    		System.out.println("Opening app");
    		driver.startActivity(MB_PKG, MB_ACTIVITY);    		
    		System.out.println("App lunched successfully");
    	}
    	return driver;	
    }
    
    
    
    
    public AppiumDriverLocalService driverservice(){
    	AppiumDriverLocalService service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().withStartUpTimeOut(30, TimeUnit.SECONDS));
		return service;
    	
    }
    
    
}
